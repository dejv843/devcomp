$('#design').click(function(){
	$('.design').addClass("active");
	$('.resp').removeClass("active");
	$('.wp').removeClass("active");
	$('.seo').removeClass("active");
	$('.design-content').removeClass("d-none");
	$('.resp-content').addClass("d-none");
	$('.wp-content').addClass("d-none");
	$('.seo-content').addClass("d-none");
});

$('#resp').click(function(){
	$('.resp').addClass("active");
	$('.design').removeClass("active");
	$('.wp').removeClass("active");
	$('.seo').removeClass("active");
	$('.resp-content').removeClass("d-none");
	$('.design-content').addClass("d-none");
	$('.wp-content').addClass("d-none");
	$('.seo-content').addClass("d-none");
});

$('#wp').click(function(){
	$('.wp').addClass("active");
	$('.design').removeClass("active");
	$('.resp').removeClass("active");
	$('.seo').removeClass("active");
	$('.wp-content').removeClass("d-none");
	$('.design-content').addClass("d-none");
	$('.resp-content').addClass("d-none");
	$('.seo-content').addClass("d-none");
});

$('#seo').click(function(){
	$('.seo').addClass("active");
	$('.design').removeClass("active");
	$('.wp').removeClass("active");
	$('.resp').removeClass("active");
	$('.seo-content').removeClass("d-none");
	$('.design-content').addClass("d-none");
	$('.wp-content').addClass("d-none");
	$('.resp-content').addClass("d-none");
});





var options = {
useEasing: true, 
useGrouping: true, 
separator: ',', 
decimal: '.', 
};

var comp = new CountUp('how-comp', 0, 124, 0, 4, options);
if (!comp.error) {
comp.start();
} else {
console.error(comp.error);
}
var clients = new CountUp('how-clients', 0, 98, 0, 4, options);
if (!clients.error) {
clients.start();
} else {
console.error(clients.error);
}
var code = new CountUp('how-code', 0, 9999, 0, 4, options);
if (!code.error) {
code.start();
} else {
console.error(code.error);
}



$('#numbers').bind('inview', function (event, visible) {
  if (visible == true) {
    console.log("OK");
  } else {
    console.log("NO");
  }
});